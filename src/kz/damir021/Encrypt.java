package kz.damir021;

public class Encrypt {
    private int key = 20;
    public String outPutText = "";


    public void encryptText(String text) {
        char[] myArray = new char[text.length()];
        outPutText = "";
        for (int i = 0; i < text.length(); i++){
            char symbolChar = text.charAt(i);
            int symbolNumber = (int)symbolChar;

            symbolNumber += (i % key);
            outPutText += (char)symbolNumber;
        }
    }
}
