package kz.damir021;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI extends JFrame {
    private JButton button = new JButton("Зашифровать");
    private JButton button2 = new JButton("Расшифровать");
    private JButton button3 = new JButton("Очистить поля");
    private JTextArea input = new JTextArea();
    private JTextArea input2 = new JTextArea();
    private JLabel label = new JLabel("Исходный текст:");
    private JLabel label2 = new JLabel("Конечный текст:");
    private JPanel mainPannel = new JPanel();
    private JPanel textAreaPannel = new JPanel();
    private JPanel textAreaPannel2 = new JPanel();
    private JPanel buttonsPannelAll = new JPanel();
    private JPanel buttonsPannel = new JPanel();
    private JPanel buttonsPannel2 = new JPanel();



    public GUI () {
        super("Программа шифрования");
        Image icon = new ImageIcon(this.getClass().getResource("/logo/logo.png")).getImage();
        this.setIconImage(icon);


        this.setBounds(100,100,500,500);
        this.setMinimumSize(new Dimension(500, 500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        input.setLineWrap(true);
        input.setWrapStyleWord(true);

        button.addActionListener(new EncryptEvent());
        button2.addActionListener(new DecryptEvent());
        mainPannel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainPannel.setLayout(new GridLayout(0,1));

        input.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        input2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        input2.setEditable(false);


        textAreaPannel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        textAreaPannel.setLayout(new GridLayout(0,2));
        textAreaPannel.add(label);
        textAreaPannel.add(input);
        JScrollPane inputScroll = new JScrollPane(input, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        textAreaPannel.add(inputScroll);

        textAreaPannel2.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        textAreaPannel2.setLayout(new GridLayout(0,2));
        textAreaPannel2.add(label2);
        textAreaPannel2.add(input2);
        JScrollPane inputScroll2 = new JScrollPane(input2, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        textAreaPannel2.add(inputScroll2);

        buttonsPannel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        buttonsPannel.setLayout(new GridLayout(0,1));

        buttonsPannel2.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        buttonsPannel2.setLayout(new GridLayout(0,1));

        buttonsPannelAll.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        buttonsPannelAll.setLayout(new GridLayout(2,1));


        buttonsPannel.add(button);
        buttonsPannel2.add(button2);

        buttonsPannelAll.add(buttonsPannel);
        buttonsPannelAll.add(buttonsPannel2);


        mainPannel.add(textAreaPannel);
        mainPannel.add(textAreaPannel2);
        mainPannel.add(buttonsPannelAll);


        Container container = this.getContentPane();

        container.add(mainPannel);



    }

    Decrypt decryptMetods = new Decrypt();
    Encrypt encryptMethods = new Encrypt();


    class EncryptEvent implements ActionListener {
        public void actionPerformed (ActionEvent e){
            String text = input.getText();
            if(text.length() == 0){
                JOptionPane.showMessageDialog(null, "Введите текст", "Error", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            encryptMethods.encryptText(text);
            input2.setText(encryptMethods.outPutText);

        }
    }
    class DecryptEvent implements ActionListener {
        public void actionPerformed (ActionEvent e){
            String text = input.getText();
            if(text.length() == 0){
                JOptionPane.showMessageDialog(null, "Введите текст", "Error", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            decryptMetods.decriptText(text);
            input2.setText(decryptMetods.outPutText);
        }
    }
}
